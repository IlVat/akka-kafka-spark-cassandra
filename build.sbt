import sbt.ExclusionRule

name := "akka-kafka-spark-cassandra"

version := "1.0"

scalaVersion := "2.11.11"


libraryDependencies ++= Seq(
  "com.typesafe.akka"     %%  "akka-http"                         % "10.0.6",
  "com.typesafe.akka"     %%  "akka-stream-kafka"                 % "0.16",
  "org.apache.spark"      %   "spark-streaming-kafka-0-10_2.11"   % "2.1.0",
  "org.apache.spark"      %   "spark-streaming_2.11"              % "2.1.0",
  "de.heikoseeberger"     %%  "akka-http-play-json"               % "1.15.0"
)

dependencyOverrides ++= Set("com.fasterxml.jackson.core" % "jackson-databind" % "2.6.5")
        
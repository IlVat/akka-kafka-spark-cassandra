package routes

import java.util.UUID

import akka.Done
import akka.actor.ActorSystem
import akka.http.scaladsl.server.Directives._

import de.heikoseeberger.akkahttpplayjson.PlayJsonSupport
import kafkaservices.KafkaProducer
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import play.api.libs.functional.syntax._
import play.api.libs.json._
import routes.UserData.Events
import routes.UserData.Events._

class UserActivityRoute(kafkaService: KafkaProducer)(implicit system: ActorSystem) extends PlayJsonSupport {

  import UserDataHelper._

  val route = path("save") {
    post {
      entity(as[UserData]) { data => println(data)
        onSuccess(kafkaService.sendToKafka(data)) { y: Done => complete("Done") }
      }
    }
  }
}

case class UserData(
    id: UUID = UUID.randomUUID(),
    categoryId: UUID,
    userId: UUID,
    event: Event,
    createdAt: DateTime = DateTime.now)

object UserData {

  object Events {

    sealed trait Event {
      def name: String
    }

    case object AddedToCard extends Event {
      val name = "addedToCard"
    }

    case object Viewed extends Event {
      val name = "viewed"
    }

    case object Search extends Event {
      val name = "search"
    }

    def getEvent: PartialFunction[String, Event] = {
      case AddedToCard.name => AddedToCard
      case Viewed.name => Viewed
      case Search.name => Search
    }
  }

}

object UserDataHelper {

  implicit object DateTimeReader extends Reads[DateTime] {
    override def reads(json: JsValue): JsResult[DateTime] = {
      json.validate[String].map[DateTime](dtString =>
        DateTime.parse(dtString, DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ"))
      )
    }
  }

  implicit object DateTimeWriter extends Writes[DateTime] {
    override def writes(o: DateTime): JsValue = JsString(o.toString())
  }

  implicit object EventWriter extends Writes[Event] {
    override def writes(o: Event): JsValue = JsString(o.name)
  }

  implicit object EventReader extends Reads[Event] {
    override def reads(json: JsValue): JsResult[Event] = {
      json.validate[String].map {
        Events.getEvent
      }
    }
  }

  implicit val userDataReader: Reads[UserData] = (
    (__ \ 'id).read[UUID] and
      (__ \ 'categoryId).read[UUID] and
      (__ \ 'userId).read[UUID] and
      (__ \ 'event).read[Event] and
      (__ \ 'createdAt).read[DateTime]
    ) (UserData.apply _)

  implicit val userDataWriter: Writes[UserData] = (
    (__ \ 'id).write[UUID] and
      (__ \ 'categoryId).write[UUID] and
      (__ \ 'userId).write[UUID] and
      (__ \ 'event).write[Event] and
      (__ \ 'createdAt).write[DateTime]
    ) (u => (u.id, u.companyId, u.userId, u.event, u.createdAt))

}
package kafkaservices

import scala.concurrent.Future

import akka.Done
import akka.actor.ActorSystem
import akka.kafka.scaladsl.Producer
import akka.kafka.{ProducerMessage, ProducerSettings}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Sink, Source}

import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.{ByteArraySerializer, StringSerializer}
import routes.UserData

class KafkaProducer(implicit system: ActorSystem, mat: ActorMaterializer) {

  val producerSettings = ProducerSettings(system, new ByteArraySerializer, new StringSerializer)
    .withBootstrapServers("localhost:9092")

  def sendToKafka(data: UserData): Future[Done] = Source.single(data)
    .map { n => println(n)
      val partition = 0
      ProducerMessage.Message(new ProducerRecord[Array[Byte], String](
        "topicA", partition, null, n.toString
      ), n.toString)
    }
    .via(Producer.flow(producerSettings))
    .map { result =>
      val record = result.message.record
      println(s"${record.topic}/${record.partition} ${result.offset}: ${record.value}" +
        s"(${result.message.passThrough})")
      result
    }
    .runWith(Sink.ignore)
}

object KafkaProducer {
  def apply()(implicit system: ActorSystem, mat: ActorMaterializer): KafkaProducer = new KafkaProducer()(system, mat)
}

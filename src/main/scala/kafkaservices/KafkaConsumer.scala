package kafkaservices


import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.streaming.kafka010.{ConsumerStrategies, KafkaUtils, _}
import org.apache.spark.SparkConf
import org.apache.spark.streaming.{Seconds, StreamingContext}

class KafkaConsumer {

  val kafkaParams = Map[String, Object](
    "bootstrap.servers" -> "localhost:9092",
    "key.deserializer" -> classOf[StringDeserializer],
    "value.deserializer" -> classOf[StringDeserializer],
    "group.id" -> "use_a_separate_group_id_for_each_stream",
    "auto.offset.reset" -> "latest",
    "auto.offset.commit" -> (false: java.lang.Boolean)
  )

  val topics = Array("topicA")

  def start(ssc: StreamingContext) = {
    val stream = KafkaUtils.createDirectStream[String, String](
      ssc,
      LocationStrategies.PreferConsistent,
      ConsumerStrategies.Subscribe[String, String](topics, kafkaParams)
    )

    stream.map { record =>
      println(record.key, record.value)
      (record.key, record.value)
    }.print()

    ssc.start()
  }
}

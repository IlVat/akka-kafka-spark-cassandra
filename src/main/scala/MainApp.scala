import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer

import kafkaservices.{KafkaConsumer, KafkaProducer}
import org.apache.spark.SparkConf
import org.apache.spark.streaming.{Seconds, StreamingContext}
import routes.UserActivityRoute

object MainApp {
  
  def main(args: Array[String]): Unit = {
    implicit val system = ActorSystem()
    implicit val materializer = ActorMaterializer()
    implicit val ec = system.dispatcher
  
    val route = new UserActivityRoute(KafkaProducer()).route
    
    val bindingFuture = Http().bindAndHandle(route, "localhost", 9000)
    
    println(s"Server online at http://localhost:9000/")

    val conf = new SparkConf().setMaster("local[2]").setAppName("KafkaConsumer")
    val ssc = new StreamingContext(conf, Seconds(1))

    val kc = new KafkaConsumer
    kc.start(ssc)

    ssc.awaitTermination()
  }
  
}
